﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoKiosk.BlazorUI.Hubs
{
    public class HubUI : Hub
    {
        public async Task MessageForRefresh(int id)
        {
            await Clients.All.SendAsync("RefreshCarousel", id);
        }
    }
}
